# BasicTransitionVB

<div align="center">
<img src="vb.png"/>
</div>

Imports Kitchanismo

Public Class Form1
    Dim transition As New KitchanismoTransition

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        InitializePanel()
        InitializeTransition()
    End Sub


    Private Sub InitializePanel()
        Dim origin As New Point(0, 0)

        PanelGreen.Location = origin
        PanelIndigo.Location = origin
        PanelGold.Location = origin
        PanelRed.Location = origin

    End Sub

    Private Sub InitializeTransition()
        transition.TabArray(PanelGreen, PanelGold, PanelIndigo, PanelRed)
        transition.Speed = 1000
        transition.Ease = Easing.CubicInOut
        transition.Type = TypeTransition.Swap
        transition.Shift = ShiftTransition.Y
        transition.ReverseShift = True
    End Sub


    Private Sub BtnGreen_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnGreen.Click

        transition.Run(PanelGreen)
    End Sub

    Private Sub BtnIndigo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnIndigo.Click
        transition.Run(PanelIndigo)
    End Sub

    Private Sub BtnGold_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnGold.Click

        transition.Run(PanelGold)
    End Sub

    Private Sub BtnRed_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnRed.Click

        transition.Run(PanelRed)
    End Sub
End Class


