﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BasicUsageUserControls
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.PanelContainer = New System.Windows.Forms.Panel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.BtnGold = New System.Windows.Forms.Button()
        Me.BtnIndigo = New System.Windows.Forms.Button()
        Me.BtnGreen = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'PanelContainer
        '
        Me.PanelContainer.BackColor = System.Drawing.Color.White
        Me.PanelContainer.Location = New System.Drawing.Point(27, 89)
        Me.PanelContainer.Name = "PanelContainer"
        Me.PanelContainer.Size = New System.Drawing.Size(799, 466)
        Me.PanelContainer.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Century Gothic", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(20, 29)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(137, 25)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "UserControls"
        '
        'BtnGold
        '
        Me.BtnGold.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnGold.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnGold.ForeColor = System.Drawing.Color.Gold
        Me.BtnGold.Location = New System.Drawing.Point(704, 12)
        Me.BtnGold.Name = "BtnGold"
        Me.BtnGold.Size = New System.Drawing.Size(122, 61)
        Me.BtnGold.TabIndex = 7
        Me.BtnGold.Text = "Gold"
        Me.BtnGold.UseVisualStyleBackColor = True
        '
        'BtnIndigo
        '
        Me.BtnIndigo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnIndigo.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnIndigo.ForeColor = System.Drawing.Color.Indigo
        Me.BtnIndigo.Location = New System.Drawing.Point(576, 12)
        Me.BtnIndigo.Name = "BtnIndigo"
        Me.BtnIndigo.Size = New System.Drawing.Size(122, 61)
        Me.BtnIndigo.TabIndex = 6
        Me.BtnIndigo.Text = "Indigo"
        Me.BtnIndigo.UseVisualStyleBackColor = True
        '
        'BtnGreen
        '
        Me.BtnGreen.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnGreen.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnGreen.ForeColor = System.Drawing.Color.MediumSeaGreen
        Me.BtnGreen.Location = New System.Drawing.Point(448, 12)
        Me.BtnGreen.Name = "BtnGreen"
        Me.BtnGreen.Size = New System.Drawing.Size(122, 61)
        Me.BtnGreen.TabIndex = 5
        Me.BtnGreen.Text = "Green"
        Me.BtnGreen.UseVisualStyleBackColor = True
        '
        'BasicUsageUserControls
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(852, 580)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.BtnGold)
        Me.Controls.Add(Me.BtnIndigo)
        Me.Controls.Add(Me.BtnGreen)
        Me.Controls.Add(Me.PanelContainer)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "BasicUsageUserControls"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "BasicUsageUserControls"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents PanelContainer As Panel
    Friend WithEvents Label1 As Label
    Friend WithEvents BtnGold As Button
    Friend WithEvents BtnIndigo As Button
    Friend WithEvents BtnGreen As Button
End Class
