﻿Imports Kitchanismo

Public Class BasicUsageUserControls

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        InitializeTransition()
    End Sub

    Dim _transition As New KitchanismoTransition
    Dim _gold As New GoldView
    Dim _indigo As New IndigoView
    Dim _green As New GreenView

    Private Sub InitializeTransition()
        Try
            _transition.Controls(_gold, _indigo, _green)
            _transition.Container(PanelContainer)
            _transition.Speed = 800
            _transition.Ease = Easing.CubicInOut
            _transition.Type = Types.Push
            _transition.Shift = Shift.XY
            _transition.ReverseShift = False
            _transition.EnableFade = True
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub BtnGreen_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnGreen.Click
        _transition.Run(_green)
    End Sub

    Private Sub BtnIndigo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnIndigo.Click
        _transition.Run(_indigo)
    End Sub

    Private Sub BtnGold_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnGold.Click
        _transition.Run(_gold)
    End Sub


End Class