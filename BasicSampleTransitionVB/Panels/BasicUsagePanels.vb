﻿Imports Kitchanismo

Public Class BasicUsagePanels
    Dim _transition As New KitchanismoTransition

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        InitializeTransition()
    End Sub

    Private Sub InitializeTransition()
        Try
            _transition.Controls(PanelGreen, PanelGold, PanelIndigo)
            _transition.Container(PanelContainer)
            '_transition.Speed = 1000
            '_transition.Ease = Easing.CubicInOut
            '_transition.Type = Types.Swap
            '_transition.Shift = Shift.Y
            '_transition.ReverseShift = True
            '_transition.EnableFade = True
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub BtnGreen_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnGreen.Click
        _transition.Run(PanelGreen)
    End Sub

    Private Sub BtnIndigo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnIndigo.Click
        _transition.Run(PanelIndigo)
    End Sub

    Private Sub BtnGold_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnGold.Click
        _transition.Run(PanelGold)
    End Sub

End Class
